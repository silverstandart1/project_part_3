# ---------------------------------------------------------------------------------------
# Part3 Step1 task2 / 2022_06_26 / ANa
# ---------------------------------------------------------------------------------------
#----------------------------------------------------------------------- Installation logstash

wget https://gist.githubusercontent.com/Yegorov/dc61c42aa4e89e139cd8248f59af6b3e/raw/20ac954e202fe6a038c2b4bb476703c02fe0df87/ya.py
chmod +x ya.py

# my link into yandex disk logstash-8.1.3-amd64.deb
./ya.py https://disk.yandex.ru/d/Ea5j0NHvkFCbrw .

sudo dpkg -i logstash-8.1.3-amd64.deb

sudo cp ./3_andrey.conf /etc/logstash/conf.d/andrey.conf

#validation
#sudo -u logstash /usr/share/logstash/bin/logstash --path.settings /etc/logstash -t
#could be run as debug
#sudo /usr/share/logstash/bin/logstash -f /etc/logstash/conf.d/andrey.conf --path.settings /etc/logstash --verbose

sudo service logstash start
sudo systemctl enable logstash
sudo service logstash restart
sudo service logstash status
