import socket
host = "10.5.0.7"
port = 9000
with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
    sock.connect((host, port))
    with open("/var/log/nginx/access.log", mode="rb") as f:
        for line in f.readlines():
            sock.send(line)
    print("all data sent to filebeat from /var/log/nginx/access.log")    
    sock.send("10.0.10.0 - ANDREYS - TEST EVENT".encode())
    print("also sent '10.0.10.0 - ANDREYS - TEST EVENT'")

