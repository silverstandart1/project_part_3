# ---------------------------------------------------------------------------------------
# Part3 Step1 task1 / 2022_06_26 / ANa
# ---------------------------------------------------------------------------------------

wget https://gist.githubusercontent.com/Yegorov/dc61c42aa4e89e139cd8248f59af6b3e/raw/20ac954e202fe6a038c2b4bb476703c02fe0df87/ya.py
chmod +x ya.py
# my link into yandex disk elasticsearch-8.1.3-amd64.deb
./ya.py https://disk.yandex.ru/d/vEgJIKT7N-rD_A .
sudo dpkg -i elasticsearch-8.1.3-amd64.deb

sudo apt-get install openjdk-11-jdk wget apt-transport-https curl gpgv gpgsm gnupg-l10n gnupg dirmngr -y
sudo chmod 777 /etc/elasticsearch
#sudo cp ./1_elasticsearch.yml /etc/elasticsearch/elasticsearch.yml
sudo cp ./1_jvm.options /etc/elasticsearch/jvm.options

#sudo systemctl daemon-reload
#sudo service elasticsearch start
#sudo service elasticsearch restart

#curl -X GET "10.5.0.7:9200/_cat/health?v=true&pretty"
#sudo netstat -tulpn

# ----- elasticsearch PROPERTIES -----
#https://sleeplessbeastie.eu/2020/07/06/how-to-display-default-elasticsearch-settings/
#curl --silent "http://10.5.0.7:9200/_cluster/settings?pretty=true"
#curl --silent "http://10.5.0.7:9200/_cluster/settings?include_defaults=true&pretty=true"
