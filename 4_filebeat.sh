# ---------------------------------------------------------------------------------------
# Part3 Step1 task2 / 2022_06_26 / ANa
# ---------------------------------------------------------------------------------------

wget https://gist.githubusercontent.com/Yegorov/dc61c42aa4e89e139cd8248f59af6b3e/raw/20ac954e202fe6a038c2b4bb476703c02fe0df87/ya.py
chmod +x ya.py

# my link into yandex disk filebeat-8.1.3-amd64.deb
./ya.py https://disk.yandex.ru/d/Oq5CzkkT3mdqmw .

sudo dpkg -i filebeat-8.1.3-amd64.deb

sudo cp ./4_filebeat.yml /etc/filebeat/filebeat.yml
sudo filebeat modules enable system
sudo filebeat modules enable nginx

sudo cp ./4_nginx.yml /etc/filebeat/modules.d/nginx.yml
sudo cp ./4_system.yml /etc/filebeat/modules.d/system.yml

sudo chmod 755 /etc/filebeat/filebeat.yml
sudo chown root:root /etc/filebeat/filebeat.yml
sudo chmod 755 /etc/filebeat/modules.d/nginx.yml
sudo chown root:root /etc/filebeat/modules.d/nginx.yml
sudo chmod 755 /etc/filebeat/modules.d/system.yml
sudo chown root:root /etc/filebeat/modules.d/system.yml

sudo filebeat setup -e -E output.logstash.enabled=true -E output.elasticsearch.enabled=false -E output.logstash.host='10.5.0.7:5044' -E setup.kibana.host='10.5.0.7:5601'

sudo service filebeat restart

curl -X GET 'http://10.5.0.7:9200/filebeat-*/_search?pretty'
