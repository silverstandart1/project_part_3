# ---------------------------------------------------------------------------------------
# Part3 Step1 task4 / 2022_06_26 / ANa
# ---------------------------------------------------------------------------------------

sudo rm /var/log/filebeat/*
echo -e "/var/log/filebeat/* CLEANED\n"

echo -e "------------------------------------------------------------------ /etc/filebeat/modules.d/nginx.yml -----------------------------------------------\n"
sudo cat /etc/filebeat/modules.d/nginx.yml
echo -e "------------------------------------------------------------------ /etc/filebeat/modules.d/system.yml -----------------------------------------------\n"
sudo cat /etc/filebeat/modules.d/system.yml
echo -e "------------------------------------------------------------------ /etc/filebeat/filebeat.yml -----------------------------------------------\n"
sudo cat /etc/filebeat/filebeat.yml
echo -e "-----------------------------------------------------------------------------------------------------------------\n"

sudo service filebeat restart

echo -e "filebeat RESTARTED\n"

sleep 5
echo -e "5 second\n"
sleep 5
echo -e "10 second\n"
sleep 5
echo -e "15 second\n"
sleep 5
netstat -tulpn | grep "9000"

echo ""


