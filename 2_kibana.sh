# ---------------------------------------------------------------------------------------
# Part3 Step1 task2 / 2022_06_26 / ANa
# ---------------------------------------------------------------------------------------

wget https://gist.githubusercontent.com/Yegorov/dc61c42aa4e89e139cd8248f59af6b3e/raw/20ac954e202fe6a038c2b4bb476703c02fe0df87/ya.py
chmod +x ya.py

# my link into yandex disk kibana-8.1.3-amd64.deb
./ya.py https://disk.yandex.ru/d/wcHt752Sbd7qPw .

sudo dpkg -i kibana-8.1.3-amd64.deb

sudo service kibana start
sudo systemctl enable kibana
sudo service kibana status

sudo chmod 777 /etc/kibana/kibana.yml
sudo cp ./2_kibana.yml /etc/kibana/kibana.yml

sudo service kibana restart
netstat -tulpn

sudo cp ./2_andrey.com.conf /etc/nginx/conf.d/andrey.com.conf
sudo nginx -t
sudo service nginx restart
