# ---------------------------------------------------------------------------------------
# Part3 Step1 task2 / 2022_06_26 / ANa
# ---------------------------------------------------------------------------------------

sudo rm /var/log/kibana/kibana.log
echo -e "/var/log/kibana/kibana.log CLEANED\n"

echo -e "------------------------------------------------------------------ /etc/kibana/kibana.yml -----------------------------------------------\n"
sudo cat /etc/kibana/kibana.yml
echo -e "------------------------------------------------------------------ /etc/nginx/conf.d/andrey.com.conf -----------------------------------------------\n"
sudo cat /etc/nginx/conf.d/andrey.com.conf
echo -e "-----------------------------------------------------------------------------------------------------------------\n"

sudo service kibana restart

echo -e "kibana RESTARTED\n"

sleep 5
echo -e "5 second\n"
sleep 5
echo -e "10 second\n"
sleep 5
echo -e "15 second\n"
sleep 5
netstat -tulpn | grep "5601"
netstat -tulpn | grep "8080"

echo -e "------------------------------------------------------------------ WARNINGS -----------------------------------------------\n"
sudo cat /var/log/kibana/kibana.log | grep "WARN"
echo -e "------------------------------------------------------------------ ERRORS -----------------------------------------------\n"
sudo cat /var/log/kibana/kibana.log | grep "ERROR"

echo ""
