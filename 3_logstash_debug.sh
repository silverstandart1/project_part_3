# ---------------------------------------------------------------------------------------
# Part3 Step1 task3 / 2022_06_26 / ANa
# ---------------------------------------------------------------------------------------

sudo rm /var/log/logstash/logstash-plain.log
echo -e "/var/log/logstash/logstash-plain.log CLEANED\n"

echo -e "------------------------------------------------------------------ /etc/logstash/conf.d/andrey.conf -----------------------------------------------\n"
sudo cat /etc/logstash/conf.d/andrey.conf
echo -e "-----------------------------------------------------------------------------------------------------------------\n"

sudo service logstash restart

echo -e "logstash RESTARTED\n"

sleep 5
echo -e "5 second\n"
sleep 5
echo -e "10 second\n"
sleep 5
echo -e "15 second\n"
sleep 5
echo -e "20 second\n"
sleep 5
echo -e "25 second\n"
sleep 5
echo -e "30 second\n"
sleep 5
echo -e "35 second\n"
sleep 5
echo -e "40 second\n"
sleep 5
echo -e "45 second\n"
sleep 5
netstat -tulpn | grep "5044"

echo -e "------------------------------------------------------------------ WARNINGS -----------------------------------------------\n"
sudo cat /var/log/logstash/logstash-plain.log | grep "WARN"
echo -e "------------------------------------------------------------------ ERRORS -----------------------------------------------\n"
sudo cat /var/log/logstash/logstash-plain.log | grep "ERROR"

echo ""

