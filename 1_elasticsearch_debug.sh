# ---------------------------------------------------------------------------------------
# Part3 Step1 task1 / 2022_06_26 / ANa
# ---------------------------------------------------------------------------------------

sudo rm /var/log/elasticsearch/elasticsearch_deprecation.json
echo -e "/var/log/elasticsearch/elasticsearch_deprecation.json CLEANED\n"

sudo rm /var/log/elasticsearch/elasticsearch.log
echo -e "/var/log/elasticsearch/elasticsearch.log CLEANED\n"

sudo rm /var/log/elasticsearch/elasticsearch_server.json
echo -e "/var/log/elasticsearch/elasticsearch_server.json CLEANED\n"

echo -e "------------------------------------------------------------------ /etc/elasticsearch/jvm.options -----------------------------------------------\n"
sudo cat /etc/elasticsearch/jvm.options
echo -e "------------------------------------------------------------------ /etc/elasticsearch/elasticsearch.yml -----------------------------------------------\n"
sudo cat /etc/elasticsearch/elasticsearch.yml
echo -e "-----------------------------------------------------------------------------------------------------------------\n"

sudo service elasticsearch restart
echo -e "elasticsearch RESTARTED\n"

sudo systemctl daemon-reload
sleep 5
echo -e "5 second\n"
sleep 5
netstat -tulpn | grep "9200"
echo -e "\n"
curl -X GET "10.5.0.7:9200/_cat/health?v=true&pretty"

echo -e "------------------------------------------------------------------ WARNINGS -----------------------------------------------\n"
sudo cat /var/log/elasticsearch/elasticsearch.log | grep "WARN"
echo -e "------------------------------------------------------------------ ERRORS -----------------------------------------------\n"
sudo cat /var/log/elasticsearch/elasticsearch.log | grep "ERROR"

echo ""
