# ---------------------------------------------------------------------------------------
# Part3 Step1 task0 / 2022_06_26 / ANa
# ---------------------------------------------------------------------------------------

#---------------------------------------------------------------------------------------------- svr installation 
#----------------------------------------------------------------------- Installation JAVA
sudo apt-get update
sudo apt-get install net-tools
sudo apt-get install openjdk-8-jdk -y
java -version

#----------------------------------------------------------------------- Installation NGINX
sudo apt-get install nginx -y
sudo service nginx start
sudo systemctl enable nginx
sleep 3
sudo service nginx status
